<?php

namespace Sonata\PageBundle\Model;

use Doctrine\Common\Persistence\ObjectManager;

/**
 * Abstract User Manager implementation which can be used as base class for your
 * concrete manager.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 */
abstract class BaseManager
{
    protected $objectManager;
    protected $class;
    protected $repository;

    /**
     * BaseManager constructor.
     * @param $class
     * @param ObjectManager $om
     */
    public function __construct($class, ObjectManager $om)
    {
        $this->class = $class;
        $this->objectManager = $om;
        $this->repository = $om->getRepository($class);

        $metadata = $om->getClassMetadata($class);
        $this->class = $metadata->getName();
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        $manager = $this->objectManager;

        if (!$manager) {
            throw new \RuntimeException(sprintf('Unable to find the mapping information for the class %s.'
                ." Please check the 'auto_mapping' option (http://symfony.com/doc/current/reference/configuration/doctrine.html#configuration-overview)"
                ." or add the bundle to the 'mappings' section in the doctrine configuration.", $this->class));
        }

        return $manager;
    }

    /**
     * {@inheritDoc}
     */
    public function getClass()
    {
        return $this->class;
    }

    public function getClassMetadata($class = null)
    {
        if (null === $class) {
            $class = $this->class;
        }

        return $metadata = $this->objectManager->getClassMetadata($class);
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function getTableName()
    {
        return $this->getClassMetadata()->getName();
    }

    public function getConnection()
    {
        return $this->getConnection();
    }

    /**
     * {@inheritdoc}
     */
    public function findAll()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * {@inheritdoc}
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->getRepository()->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * {@inheritdoc}
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->getRepository()->findOneBy($criteria, $orderBy);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return $this->getRepository()->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        return new $this->class();
    }

    public function save($entity, $andFlush = true)

    {
        $this->objectManager->persist($entity);
        $this->objectManager->flush();
    }

    public function delete($entity, $andFlush = true)

    {
        $this->objectManager->persist($entity);
        $this->objectManager->flush();
    }
}
